global _start
section .data
message db "Hello World", 10
lengthofmessage equ $-message

section .text
_start: mov EAX, 4
mov EBX, 1
mov ECX, message
mov EDX, lengthofmessage
int 80h

mov EAX, 1
mov EBX, 0
int 80h