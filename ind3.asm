;12. Ввести строку s1 длиной не менее 10 символов. Скопировать из строки s1 в строку  s2  пятый символ два раза и три раза второй.
;Вывести на экран строки s1 и s2.
global _start

section .bss

    s1 resb 15 ; 10 + 5 (Запасные байты)
    s2 times 5 resb 1


section .data
    hello db "Привет! Введите строку: "
    hellolength equ $ - hello

section .text
_start:
    mov EAX, 4 ; Выводим сообщение hello
    mov EBX, 1
    mov ECX, hello
    mov EDX, hellolength
    int 80h 

    mov EAX, 3 ; Вводим строку 1
    mov EBX, 1
    mov ECX, s1
    mov EDX, 15
    int 80h

    mov ECX, 0
    mov EAX, [s1]
    whilejump:


    mov EAX, 1 ; sys_exit()
    mov EBX, 0
    int 80h

