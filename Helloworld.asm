;https://pastebin.com/R8LDDC5f
global _start ; метка начала работы
section .data ; секция переменных
msg db "Hello World", 10 ; сообщение и символ перевода строки
msg_len equ $-msg ; длина сообщения

section .text ; секция кода
_start: mov eax, 4 ; 4 в EAX 
mov ebx, 1 ; 1 в EBX
mov ecx, msg ; строка в ECX
mov edx, msg_len ; Длина строки в EDX
int 80h ; Системное прерывание

mov eax, 1 ; 1 в EAX
mov ebx, 0 ; 0 в EBX
int 80h ; Системное прерывание
