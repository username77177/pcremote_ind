global _start
section .data
    hellomessage db "Hello, enter your number: " ; Вводное сообщение
    hmlength equ $ - hellomessage

    finalmes db "Your number is: " 
    fmlength equ $ - finalmes

section .bss
    num resb 5

section .text

_start:
    mov EAX, 4 ; Hello message
    mov EBX, 1
    mov ECX, hellomessage
    mov EDX, hmlength
    int 80h

    mov EAX, 3 ; User in
    mov EBX, 1
    mov ECX, num
    mov EDX, 5
    int 80h 

    mov EAX, 4 ; print finalmes
    mov EBX, 1
    mov ECX, finalmes
    mov EDX, fmlength
    int 80h

    mov EAX, 4 ; print userin 
    mov EBX, 1
    mov ECX, num
    mov EDX, 5
    int 80h
    
    mov EAX, 1 ; Exit
    mov EBX, 0
    int 80h